const path = require("path");
const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const request = require('request');
const multer = require('multer');
const https = require('https');
const FormData = require('form-data');
const { Readable } = require('stream');
const logger = morgan("tiny");
const axios = require('axios');
const cloud = require('wx-server-sdk')
const { createProxyMiddleware, fixRequestBody } = require('http-proxy-middleware');

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV,
})

// const isProd = process.env.NODE_ENV === 'prod';
const Host = 'https://www.esmeralda.pro';
const api = axios.create({
  baseURL: Host,
  timeout: 20000,
});
const storage = multer.memoryStorage();
const upload = multer({storage: storage});

const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(logger);
const router = express.Router()

router.use(express.json());

const ProxyHost = 'www.esmeralda.pro';
const ProxyDomain = `https://${ProxyHost}`;

const GATEWAY_SECRET = 'SDGLSDFJHB278293tuh4g2v9iKn23tg2bhgj9';
router.get('/favicon.ico', async (req, res) => {
  request(`${ProxyDomain}/favicon.ico`, {
    headers: {
      origin: ProxyHost,
    },
  }).pipe(res);
});
router.get('/qqNYGMX9N8.txt', async (req, res) => {
  res.send('1e2e532badc81cdabda862243e1cc1d9');
});
router.get('/index.html', function(req,res) {
  //modify the url in any way you want
  res.set('location', ProxyDomain);
  res.status(302).send()
});

router.post("/api/esmeralda/gateway", async (req, res) => {
  const { path, data, method } = req.body;
  console.log(method, path, data, 1221);
  const openId = req.headers["x-wx-openid"];
  if (!openId || !method) {
    res.send({
      code: -1,
      message: 'not allowed',
    })
    return;
  }
  const headers = {
    headers: {
      'x-wx-openid': openId,
      'x-gateway-secret': GATEWAY_SECRET,
    }
  };
  let ret = {};
  if (/get/i.test(method)) {
    ret = await api[method](path, {
      params: data,
      ...headers
    });
  } else {
    ret = await api[method](path, data, {
      ...headers
    });
  }
  console.log(ret.data.message, 111);
  res.send(ret.data ? ret.data : {code: -1, message: '服务器异常，请稍后再试'});
})
app.use('/', router);

app.use('/*', createProxyMiddleware({
  target: ProxyDomain,
  changeOrigin: true,
  pathFilter: ['/api/esmeralda/gateway'],
}));



const port = process.env.PORT || 80;

async function bootstrap() {
  app.listen(port, () => {
    console.log("启动成功", port);
  });
}

bootstrap();
